<?php 

/**
 * SharedController Controller
 * @category  Controller / Model
 */
class SharedController extends BaseController{
	
	/**
     * arsip_SUBKK_option_list Model Action
     * @return array
     */
	function arsip_SUBKK_option_list($lookup_KK){
		$db = $this->GetModel();
		$sqltext = "SELECT  DISTINCT SUBKK AS value,DITAMPILKAN AS label FROM kode_subklasifikasi WHERE KODEKK= ? ORDER BY KODEKK ASC"  ;
		$arr = $db->rawQuery($sqltext, array($lookup_KK));
		return $arr;
	}

	/**
     * arsip_KT_option_list Model Action
     * @return array
     */
	function arsip_KT_option_list(){
		$db = $this->GetModel();
		$sqltext = "SELECT  DISTINCT KETERANGAN AS value,KETERANGAN AS label FROM keterangan ORDER BY KETERANGAN";
		$arr = $db->rawQuery($sqltext);
		return $arr;
	}

	/**
     * arsip_ST_option_list Model Action
     * @return array
     */
	function arsip_ST_option_list(){
		$db = $this->GetModel();
		$sqltext = "SELECT  DISTINCT STATUS AS value,STATUS AS label FROM status ORDER BY STATUS ASC";
		$arr = $db->rawQuery($sqltext);
		return $arr;
	}

	/**
     * arsip_NOURITEM_option_list Model Action
     * @return array
     */
	function arsip_NOURITEM_option_list(){
		$db = $this->GetModel();
		$sqltext = "SELECT  DISTINCT KODEKK AS value,URAIANKK AS label FROM kode_kk ORDER BY KODEKK ASC";
		$arr = $db->rawQuery($sqltext);
		return $arr;
	}

	/**
     * arsip_KK_option_list Model Action
     * @return array
     */
	function arsip_KK_option_list($lookup_NOURITEM){
		$db = $this->GetModel();
		$sqltext = "SELECT  DISTINCT KODEKK AS value,TAMPILANKK AS label FROM master_kasifikasi WHERE URAIANKK= ? ORDER BY URAIANKK ASC" ;
		$arr = $db->rawQuery($sqltext, array($lookup_NOURITEM));
		return $arr;
	}

	/**
     * cetakberkas_UNITPENGOLAH_option_list Model Action
     * @return array
     */
	function cetakberkas_UNITPENGOLAH_option_list(){
		$db = $this->GetModel();
		$sqltext = "SELECT  DISTINCT UNITPENGOLAH AS value,UNITPENGOLAH AS label FROM unitpengolah ORDER BY UNITPENGOLAH ASC";
		$arr = $db->rawQuery($sqltext);
		return $arr;
	}

	/**
     * cetakberkas_PENANDATANGAN_option_list Model Action
     * @return array
     */
	function cetakberkas_PENANDATANGAN_option_list(){
		$db = $this->GetModel();
		$sqltext = "SELECT  DISTINCT NAMA AS value,NAMA AS label FROM penandatangan ORDER BY NAMA";
		$arr = $db->rawQuery($sqltext);
		return $arr;
	}

	/**
     * cetakberkas_JABATAN_option_list Model Action
     * @return array
     */
	function cetakberkas_JABATAN_option_list($lookup_PENANDATANGAN){
		$db = $this->GetModel();
		$sqltext = "SELECT  DISTINCT JABATAN AS value,JABATAN AS label FROM penandatangan WHERE NAMA= ? ORDER BY NAMA" ;
		$arr = $db->rawQuery($sqltext, array($lookup_PENANDATANGAN));
		return $arr;
	}

	/**
     * cetakberkas_NIP_option_list Model Action
     * @return array
     */
	function cetakberkas_NIP_option_list($lookup_JABATAN){
		$db = $this->GetModel();
		$sqltext = "SELECT NIP AS value,NIP AS label FROM penandatangan WHERE JABATAN= ? ORDER BY JABATAN ASC" ;
		$arr = $db->rawQuery($sqltext, array($lookup_JABATAN));
		return $arr;
	}

	/**
     * getcount_arsipinaktif Model Action
     * @return Value
     */
	function getcount_arsipinaktif(){
		$db = $this->GetModel();
		$sqltext = "SELECT COUNT(*) AS num FROM arsip WHERE ST ='INAKTIF'";
		$val = $db->rawQueryValue($sqltext);
		
		if(is_array($val)){
			return $val[0];
		}
		return $val;
	}

	/**
     * getcount_arsipaktif Model Action
     * @return Value
     */
	function getcount_arsipaktif(){
		$db = $this->GetModel();
		$sqltext = "SELECT COUNT(*) AS num FROM arsip WHERE ST ='AKTIF'";
		$val = $db->rawQueryValue($sqltext);
		
		if(is_array($val)){
			return $val[0];
		}
		return $val;
	}

}
