<?php 
/**
 * Sec_users Page Controller
 * @category  Controller
 */
class Sec_usersController extends BaseController{
	/**
     * Edit single field Action 
     * Return record id
     * @return View
     */
	function editfield($rec_id = null){
		$db = $this->GetModel();
		$this->rec_id = $rec_id;
		$tablename = $this->tablename = 'sec_users';
		$fields = $this->fields = array('login','pswd','name','email','active','activation_code','priv_admin'); //editable fields
		if(is_post_request()){
			Csrf :: cross_check();
			$postdata = array();
			if(isset($_POST['name']) && isset($_POST['value'])){
				$fieldname = $_POST['name'];
				$fieldvalue = $_POST['value'];
				$postdata[$fieldname] = $fieldvalue;
				$postdata = $this->transform_request_data($postdata);
			}
			else{
				$this->view->page_error = "invalid post data";
			}
			$this->rules_array = array(
				'login' => 'required',
				'pswd' => 'required',
				'name' => 'required',
				'email' => 'required|valid_email',
				'active' => 'required',
				'activation_code' => 'required',
				'priv_admin' => 'required',
			);
			$this->sanitize_array = array(
				'login' => 'sanitize_string',
				'pswd' => 'sanitize_string',
				'name' => 'sanitize_string',
				'email' => 'sanitize_string',
				'active' => 'sanitize_string',
				'activation_code' => 'sanitize_string',
				'priv_admin' => 'sanitize_string',
			);
			$modeldata = $this -> modeldata = $this->validate_form($postdata, true);
			if(empty($this->view->page_error)){
				$db->where('sec_users.login' , $rec_id);
				try{
					$bool = $db->update($tablename, $modeldata);
					$numRows = $db->getRowCount();
					if($bool && $numRows){
						render_json(
							array(
								'num_rows' =>$numRows,
								'rec_id' =>$rec_id,
							)
						);
					}
					else{
						$page_error = null;
						if($db->getLastError()){
							$page_error = $db->getLastError();
						}
						elseif(!$numRows){
							$page_error = "No record updated";
						}
						else{
							$page_error = "No record found";
						}
						render_error($page_error);
					}
				}
				catch(Exception $e){
					render_error($e->getMessage());
				}
			}
			else{
				render_error($this->view->page_error);
			}
		}
		else{
			render_error("Request type not accepted");
		}
	}
}
