<?php 
/**
 * Sec_groups_apps Page Controller
 * @category  Controller
 */
class Sec_groups_appsController extends BaseController{
	/**
     * Edit single field Action 
     * Return record id
     * @return View
     */
	function editfield($rec_id = null){
		$db = $this->GetModel();
		$this->rec_id = $rec_id;
		$tablename = $this->tablename = 'sec_groups_apps';
		$fields = $this->fields = array('group_id','app_name','priv_access','priv_insert','priv_delete','priv_update','priv_export','priv_print'); //editable fields
		if(is_post_request()){
			Csrf :: cross_check();
			$postdata = array();
			if(isset($_POST['name']) && isset($_POST['value'])){
				$fieldname = $_POST['name'];
				$fieldvalue = $_POST['value'];
				$postdata[$fieldname] = $fieldvalue;
				$postdata = $this->transform_request_data($postdata);
			}
			else{
				$this->view->page_error = "invalid post data";
			}
			$this->rules_array = array(
				'group_id' => 'required|numeric',
				'app_name' => 'required',
				'priv_access' => 'required',
				'priv_insert' => 'required',
				'priv_delete' => 'required',
				'priv_update' => 'required',
				'priv_export' => 'required',
				'priv_print' => 'required',
			);
			$this->sanitize_array = array(
				'group_id' => 'sanitize_string',
				'app_name' => 'sanitize_string',
				'priv_access' => 'sanitize_string',
				'priv_insert' => 'sanitize_string',
				'priv_delete' => 'sanitize_string',
				'priv_update' => 'sanitize_string',
				'priv_export' => 'sanitize_string',
				'priv_print' => 'sanitize_string',
			);
			$modeldata = $this -> modeldata = $this->validate_form($postdata, true);
			if(empty($this->view->page_error)){
				$db->where('sec_groups_apps.group_id' , $rec_id)->orWhere('sec_groups_apps.app_name' , $rec_id);
				try{
					$bool = $db->update($tablename, $modeldata);
					$numRows = $db->getRowCount();
					if($bool && $numRows){
						render_json(
							array(
								'num_rows' =>$numRows,
								'rec_id' =>$rec_id,
							)
						);
					}
					else{
						$page_error = null;
						if($db->getLastError()){
							$page_error = $db->getLastError();
						}
						elseif(!$numRows){
							$page_error = "No record updated";
						}
						else{
							$page_error = "No record found";
						}
						render_error($page_error);
					}
				}
				catch(Exception $e){
					render_error($e->getMessage());
				}
			}
			else{
				render_error($this->view->page_error);
			}
		}
		else{
			render_error("Request type not accepted");
		}
	}
}
