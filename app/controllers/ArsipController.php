<?php 
/**
 * Arsip Page Controller
 * @category  Controller
 */
class ArsipController extends BaseController{
	/**
     * Load Record Action 
     * $arg1 Field Name
     * $arg2 Field Value 
     * $param $arg1 string
     * $param $arg1 string
     * @return View
     */
	function index($fieldname = null , $fieldvalue = null){
		$db = $this->GetModel();
		$tablename = $this->tablename = 'arsip';
		$fields = array('ID', 
			'NB', 
			'KK', 
			'SUBKK', 
			'UI', 
			'TGL', 
			'JMLH', 
			'KT', 
			'ST', 
			'NB AS Daftar_Isi_Berkas');
		$limit = $this->get_page_limit(200); // return pagination from BaseModel Class e.g array(5,20)
		if(!empty($this->search)){
			$text = trim($this->search);
			$db->orWhere('ID',"%$text%",'LIKE');
			$db->orWhere('NB',"%$text%",'LIKE');
			$db->orWhere('KK',"%$text%",'LIKE');
			$db->orWhere('SUBKK',"%$text%",'LIKE');
			$db->orWhere('NOMORITEM',"%$text%",'LIKE');
			$db->orWhere('NOURITEM',"%$text%",'LIKE');
			$db->orWhere('UI',"%$text%",'LIKE');
			$db->orWhere('TGL',"%$text%",'LIKE');
			$db->orWhere('JMLH',"%$text%",'LIKE');
			$db->orWhere('KT',"%$text%",'LIKE');
			$db->orWhere('ST',"%$text%",'LIKE');
			$db->orWhere('FILE',"%$text%",'LIKE');
			$db->orWhere('BARCODE',"%$text%",'LIKE');
		}
		if(!empty($this->orderby)){ // when order by request fields (from $_GET param)
			$db->orderBy($this->orderby,$this->ordertype);
		}
		else{
			$db->orderBy('arsip.ID', ORDER_TYPE);
		}
		if( !empty($fieldname) ){
			$db->where($fieldname , $fieldvalue);
		}
		//page filter command
		$tc = $db->withTotalCount();
		$records = $db->get($tablename, $limit, $fields);
		$data = new stdClass;
		$data->records = $records;
		$data->record_count = count($records);
		$data->total_records = intval($tc->totalCount);
		if($db->getLastError()){
			$page_error = $db->getLastError();
			$this->view->page_error = $page_error;
		}
		$this->view->page_title ="Arsip";
		$this->view->render('arsip/list.php' , $data ,'main_layout.php');
	}
	/**
     * Load csv|json data
     * @return data
     */
	function import_data(){
		if(!empty($_FILES['file'])){
			$finfo = pathinfo($_FILES['file']['name']);
			$ext = strtolower($finfo['extension']);
			if(!in_array($ext , array('csv','json'))){
				set_flash_msg("File format not supported",'danger');
			}
			else{
			$file_path = $_FILES['file']['tmp_name'];
				if(!empty($file_path)){
					$db = $this->GetModel();
					$tablename = $this->tablename = 'arsip';
					if($ext == 'csv'){
						$options = array('table' => $tablename, 'fields' => '', 'delimiter' => ',', 'quote' => '"');
						$data = $db->loadCsvData( $file_path , $options , false );
					}
					else{
						$data = $db->loadJsonData( $file_path, $tablename , false );
					}
					if($db->getLastError()){
						set_flash_msg($db->getLastError(),'danger');
					}
					else{
						set_flash_msg("Data imported successfully",'success');
					}
				}
				else{
					set_flash_msg("Error uploading file",'success');
				}
			}
		}
		else{
			set_flash_msg("No file selected for upload",'warning');
		}
		$list_page = (!empty($_POST['redirect']) ? $_POST['redirect'] : 'arsip/list');
		redirect_to_page($list_page);
	}
	/**
     * View Record Action 
     * @return View
     */
	function view( $rec_id = null , $value = null){
		$db = $this->GetModel();
		$this->rec_id = $rec_id;
		$tablename = $this->tablename = 'arsip';
		$fields = array('arsip.ID', 
			'arsip.NB', 
			'arsip.KK', 
			'arsip.SUBKK', 
			'arsip.NOMORITEM', 
			'arsip.NOURITEM', 
			'arsip.UI', 
			'arsip.TGL', 
			'daftar_berkas.NB AS daftar_berkas_NB', 
			'arsip.JMLH', 
			'arsip.KT', 
			'arsip.ST', 
			'arsip.FILE', 
			'arsip.BARCODE');
		if( !empty($value) ){
			$db->where($rec_id, urldecode($value));
		}
		else{
			$db->where('arsip.ID' , $rec_id);
		}
		$db->join("daftar_berkas","arsip.NB = daftar_berkas.NB","INNER");  
		$record = $db->getOne($tablename, $fields );
		if(!empty($record)){
			$this->view->page_title ="View  Arsip";
			$this->view->render('arsip/view.php' , $record ,'main_layout.php');
		}
		else{
			$page_error = null;
			if($db->getLastError()){
				$page_error = $db->getLastError();
			}
			else{
				$page_error = "No record found";
			}
			$this->view->page_error = $page_error;
			$this->view->render('arsip/view.php' , $record , 'main_layout.php');
		}
	}
	/**
     * Add New Record Action 
     * If Not $_POST Request, Display Add Record Form View
     * @return View
     */
	function add(){
		if(is_post_request()){
			Csrf :: cross_check();
			$db = $this->GetModel();
			$tablename = $this->tablename = 'arsip';
			$fields = $this->fields = array('NB','NOURITEM','KK','SUBKK','UI','TGL','JMLH','KT','ST'); //insert fields
			$postdata = $this->transform_request_data($_POST);
			$modeldata = $this -> modeldata = $postdata;
			if(empty($this->view->page_error)){
				$rec_id = $this->rec_id = $db->insert($tablename, $modeldata);
				if(!empty($rec_id)){
					if(is_ajax()){
						render_json("Record added successfully");
					}
					else{
						set_flash_msg("Arsip Berhasil di Tambah",'success');
						redirect_to_page("arsip");
					}
					return;
				}
				else{
					$page_error = null;
					if($db->getLastError()){
						$page_error = $db->getLastError();
					}
					else{
						$page_error = "Error inserting record";
					}
					if(is_ajax()){
						render_error($page_error); 
						return;
					}
					else{
						$this->view->page_error[] = $page_error;
					}
				}
			}
		}
		$this->view->page_title ="Add New Arsip";
		$this->view->render('arsip/add.php' ,null,'main_layout.php');
	}
	/**
     * Edit Record Action 
     * If Not $_POST Request, Display Edit Record Form View
     * @return View
     */
	function edit($rec_id = null){
		$db = $this->GetModel();
		$this->rec_id = $rec_id;
		$tablename = $this->tablename = 'arsip';
		$fields = $this->fields = array('ID','NB','NOURITEM','KK','SUBKK','UI','TGL','JMLH','KT','ST'); //editable fields
		if(is_post_request()){
			Csrf :: cross_check();
			$postdata = $this->transform_request_data($_POST);
			$this->rules_array = array(
				'NB' => 'required',
				'NOURITEM' => 'required',
				'KK' => 'required',
				'SUBKK' => 'required',
				'UI' => 'required',
				'TGL' => 'required',
				'JMLH' => 'numeric',
				'KT' => 'required',
				'ST' => 'required',
			);
			$this->sanitize_array = array(
				'NB' => 'sanitize_string',
				'NOURITEM' => 'sanitize_string',
				'KK' => 'sanitize_string',
				'SUBKK' => 'sanitize_string',
				'TGL' => 'sanitize_string',
				'JMLH' => 'sanitize_string',
				'KT' => 'sanitize_string',
				'ST' => 'sanitize_string',
			);
			$modeldata = $this -> modeldata = $this->validate_form($postdata);
			if(empty($this->view->page_error)){
				$db->where('arsip.ID' , $rec_id);
				$bool = $db->update($tablename, $modeldata);
				$numRows = $db->getRowCount(); //number of affected rows. 0 = no record field updated
				if($bool && $numRows){
					if(is_ajax()){
						render_json("Record updated successfully");
					}
					else{
						set_flash_msg('','');
						redirect_to_page("arsip");
					}
					return;
				}
				else{
					$page_error = null;
					if($db->getLastError()){
						$page_error = $db->getLastError();
					}
					elseif(!$numRows){
						$page_error = "No record updated";
						if(is_ajax()){
							render_error($page_error); //return http status error
						}
						else{
							//no changes made to the table record
							set_flash_msg($page_error, 'warning');
							redirect_to_page("arsip");
						}
						return;
					}
					else{
						$page_error = "No record found";
					}
					if(is_ajax()){
						render_error($page_error); //return http status error
						return;
					}
					//continue to display edit page with errors
					$this->view->page_error[] = $page_error;
				}
			}
		}
		$db->where('arsip.ID' , $rec_id);
		$data = $db->getOne($tablename, $fields);
		$this->view->page_title ="Edit  Arsip";
		if(!empty($data)){
			$this->view->render('arsip/edit.php' , $data, 'main_layout.php');
		}
		else{
			if($db->getLastError()){
				$this->view->page_error[] = $db->getLastError();
			}
			else{
				$this->view->page_error[] = "No record found";
			}
			$this->view->render('arsip/edit.php' , $data , 'main_layout.php');
		}
	}
	/**
     * Edit single field Action 
     * Return record id
     * @return View
     */
	function editfield($rec_id = null){
		$db = $this->GetModel();
		$this->rec_id = $rec_id;
		$tablename = $this->tablename = 'arsip';
		$fields = $this->fields = array('ID','NB','NOURITEM','KK','SUBKK','UI','TGL','JMLH','KT','ST'); //editable fields
		if(is_post_request()){
			Csrf :: cross_check();
			$postdata = array();
			if(isset($_POST['name']) && isset($_POST['value'])){
				$fieldname = $_POST['name'];
				$fieldvalue = $_POST['value'];
				$postdata[$fieldname] = $fieldvalue;
				$postdata = $this->transform_request_data($postdata);
			}
			else{
				$this->view->page_error = "invalid post data";
			}
			$this->rules_array = array(
				'NB' => 'required',
				'NOURITEM' => 'required',
				'KK' => 'required',
				'SUBKK' => 'required',
				'UI' => 'required',
				'TGL' => 'required',
				'JMLH' => 'numeric',
				'KT' => 'required',
				'ST' => 'required',
			);
			$this->sanitize_array = array(
				'NB' => 'sanitize_string',
				'NOURITEM' => 'sanitize_string',
				'KK' => 'sanitize_string',
				'SUBKK' => 'sanitize_string',
				'TGL' => 'sanitize_string',
				'JMLH' => 'sanitize_string',
				'KT' => 'sanitize_string',
				'ST' => 'sanitize_string',
			);
			$modeldata = $this -> modeldata = $this->validate_form($postdata, true);
			if(empty($this->view->page_error)){
				$db->where('arsip.ID' , $rec_id);
				try{
					$bool = $db->update($tablename, $modeldata);
					$numRows = $db->getRowCount();
					if($bool && $numRows){
						render_json(
							array(
								'num_rows' =>$numRows,
								'rec_id' =>$rec_id,
							)
						);
					}
					else{
						$page_error = null;
						if($db->getLastError()){
							$page_error = $db->getLastError();
						}
						elseif(!$numRows){
							$page_error = "No record updated";
						}
						else{
							$page_error = "No record found";
						}
						render_error($page_error);
					}
				}
				catch(Exception $e){
					render_error($e->getMessage());
				}
			}
			else{
				render_error($this->view->page_error);
			}
		}
		else{
			render_error("Request type not accepted");
		}
	}
	/**
     * Delete Record Action 
     * @return View
     */
	function delete( $rec_ids = null ){
		Csrf :: cross_check();
		$db = $this->GetModel();
		$this->rec_id = $rec_ids;
		$tablename = $this->tablename = 'arsip';
		//split record id separated by comma into array
		$arr_id = explode(',', $rec_ids);
		//set query conditions for all records that will be deleted
		foreach($arr_id as $rec_id){
			$db->where('arsip.ID' , $rec_id,"=",'OR');
		}
		$bool = $db->delete($tablename);
		if($bool){
			set_flash_msg('','');
		}
		else{
			$page_error = "";
			if($db->getLastError()){
				$page_error = $db->getLastError();
			}
			else{
				$page_error = "Error deleting the record. please make sure that the record exit";
			}
			set_flash_msg($page_error,'danger');
		}
		redirect_to_page("arsip");
	}
}
