<?php 
/**
 * Kode_klasifikasi Page Controller
 * @category  Controller
 */
class Kode_klasifikasiController extends BaseController{
	/**
     * Edit single field Action 
     * Return record id
     * @return View
     */
	function editfield($rec_id = null){
		$db = $this->GetModel();
		$this->rec_id = $rec_id;
		$tablename = $this->tablename = 'kode_klasifikasi';
		$fields = $this->fields = array('ID','KODEKK','URAIANKK','SUBKK','URAIANSUBKK','DITAMPILKAN','field_F'); //editable fields
		if(is_post_request()){
			Csrf :: cross_check();
			$postdata = array();
			if(isset($_POST['name']) && isset($_POST['value'])){
				$fieldname = $_POST['name'];
				$fieldvalue = $_POST['value'];
				$postdata[$fieldname] = $fieldvalue;
				$postdata = $this->transform_request_data($postdata);
			}
			else{
				$this->view->page_error = "invalid post data";
			}
			$this->rules_array = array(
				'KODEKK' => 'required',
				'URAIANKK' => 'required',
				'SUBKK' => 'required',
				'URAIANSUBKK' => 'required',
				'DITAMPILKAN' => 'required',
				'field_F' => 'required',
			);
			$this->sanitize_array = array(
				'KODEKK' => 'sanitize_string',
				'URAIANKK' => 'sanitize_string',
				'SUBKK' => 'sanitize_string',
				'URAIANSUBKK' => 'sanitize_string',
				'DITAMPILKAN' => 'sanitize_string',
				'field_F' => 'sanitize_string',
			);
			$modeldata = $this -> modeldata = $this->validate_form($postdata, true);
			if(empty($this->view->page_error)){
				$db->where('kode_klasifikasi.ID' , $rec_id);
				try{
					$bool = $db->update($tablename, $modeldata);
					$numRows = $db->getRowCount();
					if($bool && $numRows){
						render_json(
							array(
								'num_rows' =>$numRows,
								'rec_id' =>$rec_id,
							)
						);
					}
					else{
						$page_error = null;
						if($db->getLastError()){
							$page_error = $db->getLastError();
						}
						elseif(!$numRows){
							$page_error = "No record updated";
						}
						else{
							$page_error = "No record found";
						}
						render_error($page_error);
					}
				}
				catch(Exception $e){
					render_error($e->getMessage());
				}
			}
			else{
				render_error($this->view->page_error);
			}
		}
		else{
			render_error("Request type not accepted");
		}
	}
}
