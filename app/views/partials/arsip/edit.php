
<?php
$comp_model = new SharedController;

$csrf_token = Csrf :: $token;

$data = $this->view_data;

//$rec_id = $data['__tableprimarykey'];
$page_id = Router :: $page_id;

$show_header = $this->show_header;
$view_title = $this->view_title;
$redirect_to = $this->redirect_to;

?>

<section class="page">
    
    <?php
    if( $show_header == true ){
    ?>
    
    <div  class="bg-light p-3 mb-3">
        <div class="container">
            
            <div class="row ">
                
                <div class="col-12 ">
                    <h3 class="record-title">Edit  Arsip</h3>
                    
                </div>
                
            </div>
        </div>
    </div>
    
    <?php
    }
    ?>
    
    <div  class="">
        <div class="container">
            
            <div class="row ">
                
                <div class="col-md-7 comp-grid">
                    
                    <?php $this :: display_page_errors(); ?>
                    
                    <div  class=" animated fadeIn">
                        <form novalidate  id="" role="form" enctype="multipart/form-data"  class="form form-horizontal needs-validation" action="<?php print_link("arsip/edit/$page_id/?csrf_token=$csrf_token"); ?>" method="post">
                            <div>
                                
                                
                                <div class="form-group ">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <label class="control-label" for="NB">Nb <span class="text-danger">*</span></label>
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="">
                                                <input id="ctrl-NB"  value="<?php  echo $data['NB']; ?>" type="text" placeholder="Enter Nb"  required="" name="NB"  class="form-control " />
                                                    
                                                    
                                                    
                                                </div>
                                                
                                                
                                            </div>
                                        </div>
                                    </div>
                                    
                                    
                                    
                                    
                                    <div class="form-group ">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <label class="control-label" for="NOURITEM">Kode Klasifikasi <span class="text-danger">*</span></label>
                                            </div>
                                            <div class="col-sm-8">
                                                <div class="">
                                                    
                                                    <select required=""  id="ctrl-NOURITEM" name="NOURITEM" data-load-path="<?php print_link('api/json/arsip_KK_option_list') ?>"
                                                        data-load-select-options="#ctrl-KK"  placeholder="Select a value ..."    class="custom-select" >
                                                        
                                                        <option value="">Select a value ...</option>
                                                        
                                                        
                                                        <?php
                                                        $rec = $data['NOURITEM'];
                                                        $NOURITEM_options = $comp_model -> arsip_NOURITEM_option_list();
                                                        if(!empty($NOURITEM_options)){
                                                        foreach($NOURITEM_options as $arr){
                                                        $val = array_values($arr);
                                                        $selected = ( $val[0] == $rec ? 'selected' : null );
                                                        ?>
                                                        <option 
                                                            <?php echo $selected; ?> value="<?php echo $val[0]; ?>"><?php echo (!empty($val[1]) ? $val[1] : $val[0]); ?>
                                                        </option>
                                                        <?php
                                                        }
                                                        }
                                                        ?>
                                                        
                                                    </select>
                                                    
                                                    
                                                </div>
                                                
                                                
                                            </div>
                                        </div>
                                    </div>
                                    
                                    
                                    
                                    
                                    <div class="form-group ">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <label class="control-label" for="KK">Kalsifikasi <span class="text-danger">*</span></label>
                                            </div>
                                            <div class="col-sm-8">
                                                <div class="">
                                                    
                                                    <select required=""  id="ctrl-KK" name="KK" data-load-path="<?php print_link('api/json/arsip_SUBKK_option_list') ?>"
                                                        data-load-select-options="#ctrl-SUBKK"  placeholder="Select a value ..."    class="custom-select" >
                                                        
                                                        <?php
                                                        $rec = $data['KK'];
                                                        $KK_options = $comp_model -> arsip_KK_option_list($data['NOURITEM']);
                                                        if(!empty($KK_options)){
                                                        foreach($KK_options as $arr){
                                                        $val = array_values($arr);
                                                        $selected = ( $val[0] == $rec ? 'selected' : null );
                                                        ?>
                                                        <option 
                                                            <?php echo $selected; ?> value="<?php echo $val[0]; ?>"><?php echo (!empty($val[1]) ? $val[1] : $val[0]); ?>
                                                        </option>
                                                        <?php
                                                        }
                                                        }
                                                        ?>
                                                        
                                                        
                                                    </select>
                                                    
                                                    
                                                </div>
                                                
                                                
                                            </div>
                                        </div>
                                    </div>
                                    
                                    
                                    
                                    
                                    <div class="form-group ">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <label class="control-label" for="SUBKK">Sub Kalsifikasi <span class="text-danger">*</span></label>
                                            </div>
                                            <div class="col-sm-8">
                                                <div class="">
                                                    
                                                    <select required=""  id="ctrl-SUBKK" name="SUBKK"  placeholder="Select a value ..."    class="custom-select" >
                                                        
                                                        <?php
                                                        $rec = $data['SUBKK'];
                                                        $SUBKK_options = $comp_model -> arsip_SUBKK_option_list($data['KK']);
                                                        if(!empty($SUBKK_options)){
                                                        foreach($SUBKK_options as $arr){
                                                        $val = array_values($arr);
                                                        $selected = ( $val[0] == $rec ? 'selected' : null );
                                                        ?>
                                                        <option 
                                                            <?php echo $selected; ?> value="<?php echo $val[0]; ?>"><?php echo (!empty($val[1]) ? $val[1] : $val[0]); ?>
                                                        </option>
                                                        <?php
                                                        }
                                                        }
                                                        ?>
                                                        
                                                        
                                                    </select>
                                                    
                                                    
                                                </div>
                                                
                                                
                                            </div>
                                        </div>
                                    </div>
                                    
                                    
                                    
                                    
                                    <div class="form-group ">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <label class="control-label" for="UI">Uraian <span class="text-danger">*</span></label>
                                            </div>
                                            <div class="col-sm-8">
                                                <div class="">
                                                    
                                                    <textarea placeholder="Enter Uraian" id="ctrl-UI"  required="" rows="" name="UI" class="htmleditor form-control"><?php  echo $data['UI']; ?></textarea>
                                                    <!--<div class="invalid-feedback animated bounceIn text-center">Please enter text</div>-->
                                                    
                                                    
                                                </div>
                                                
                                                
                                            </div>
                                        </div>
                                    </div>
                                    
                                    
                                    
                                    
                                    <div class="form-group ">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <label class="control-label" for="TGL">Tgl <span class="text-danger">*</span></label>
                                            </div>
                                            <div class="col-sm-8">
                                                <div class="">
                                                    <input id="ctrl-TGL" class="form-control datepicker  datepicker"  required="" value="<?php  echo $data['TGL']; ?>" type="datetime" name="TGL" placeholder="Enter Tgl" data-enable-time="false" data-min-date="2013" data-max-date="" data-date-format="Y-m-d" data-alt-format="F j, Y" data-inline="false" data-no-calendar="false" data-mode="single" />
                                                        
                                                        
                                                    </div>
                                                    
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        
                                        
                                        
                                        
                                        <div class="form-group ">
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <label class="control-label" for="JMLH">Jmlh </label>
                                                </div>
                                                <div class="col-sm-8">
                                                    <div class="input-group">
                                                        <input id="ctrl-JMLH"  value="<?php  echo $data['JMLH']; ?>" type="number" placeholder="Enter Jmlh" step="1"  name="JMLH"  class="form-control " />
                                                            
                                                            
                                                            
                                                            <div class="input-group-append">
                                                                <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                                            </div>
                                                            
                                                        </div>
                                                        
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            
                                            
                                            
                                            <div class="form-group ">
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <label class="control-label" for="KT">Kt <span class="text-danger">*</span></label>
                                                    </div>
                                                    <div class="col-sm-8">
                                                        <div class="">
                                                            
                                                            <select required=""  id="ctrl-KT" name="KT"  placeholder="Select a value ..."    class="custom-select" >
                                                                
                                                                <option value="">Select a value ...</option>
                                                                
                                                                
                                                                <?php
                                                                $rec = $data['KT'];
                                                                $KT_options = $comp_model -> arsip_KT_option_list();
                                                                if(!empty($KT_options)){
                                                                foreach($KT_options as $arr){
                                                                $val = array_values($arr);
                                                                $selected = ( $val[0] == $rec ? 'selected' : null );
                                                                ?>
                                                                <option 
                                                                    <?php echo $selected; ?> value="<?php echo $val[0]; ?>"><?php echo (!empty($val[1]) ? $val[1] : $val[0]); ?>
                                                                </option>
                                                                <?php
                                                                }
                                                                }
                                                                ?>
                                                                
                                                            </select>
                                                            
                                                            
                                                        </div>
                                                        
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            
                                            
                                            
                                            <div class="form-group ">
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <label class="control-label" for="ST">St <span class="text-danger">*</span></label>
                                                    </div>
                                                    <div class="col-sm-8">
                                                        <div class="">
                                                            
                                                            <select required=""  id="ctrl-ST" name="ST"  placeholder="Select a value ..."    class="custom-select" >
                                                                
                                                                <option value="">Select a value ...</option>
                                                                
                                                                
                                                                <?php
                                                                $rec = $data['ST'];
                                                                $ST_options = $comp_model -> arsip_ST_option_list();
                                                                if(!empty($ST_options)){
                                                                foreach($ST_options as $arr){
                                                                $val = array_values($arr);
                                                                $selected = ( $val[0] == $rec ? 'selected' : null );
                                                                ?>
                                                                <option 
                                                                    <?php echo $selected; ?> value="<?php echo $val[0]; ?>"><?php echo (!empty($val[1]) ? $val[1] : $val[0]); ?>
                                                                </option>
                                                                <?php
                                                                }
                                                                }
                                                                ?>
                                                                
                                                            </select>
                                                            
                                                            
                                                        </div>
                                                        
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            
                                            
                                            
                                        </div>
                                        <div class="form-ajax-status"></div>
                                        <div class="form-group text-center">
                                            <button class="btn btn-primary" type="submit">
                                                
                                                <i class="fa fa-send"></i>
                                            </button>
                                        </div>
                                    </form>
                                </div>
                                
                            </div>
                            
                        </div>
                    </div>
                </div>
                
            </section>
            