
<?php
$comp_model = new SharedController;
$view_data = $this->view_data; //array of all  data passed from controller
$field_name = $view_data['field_name'];
$field_value = $view_data['field_value'];
$form_data = $this->form_data; //$_GET request pass to the page as form fields values


$page_id = random_str(6);
?>
<div>
    <div class="card">
        <div class="card-header p-0 pt-2 px-2">
            <ul class="nav nav-tabs">
                
                <li class="nav-item">
                    <a data-toggle="tab" href="#arsip_daftar_berkas_List_<?php echo $page_id ?>" class="nav-link active">
                        List Berkas
                    </a>
                </li>
                
                <li class="nav-item">
                    <a data-toggle="tab" href="#arsip_daftar_berkas_Add_<?php echo $page_id ?>" class="nav-link ">
                        Tambah Berkas
                    </a>
                </li>
                
            </ul>
        </div>
        <div class="card-body">
            <div class="tab-content">
                
                <div class="tab-pane fade show active show" id="arsip_daftar_berkas_List_<?php echo $page_id ?>" role="tabpanel">
                    
                    <?php $this->render_page("daftar_berkas/list/NB/$field_value"); ?>
                    
                </div>
                
                <div class="tab-pane fade show " id="arsip_daftar_berkas_Add_<?php echo $page_id ?>" role="tabpanel">
                    
                    <?php $this->render_page("daftar_berkas/add/?NB=$field_value", array('NB' => get_val('NB'),'KK' => get_val('KK'),'SUBKK' => get_val('SUBKK'),'TGL' => get_val('TGL'),'KT' => get_val('KT'),'ST' => get_val('ST'))); ?>
                    
                </div>
                
            </div>
        </div>
    </div>
</div>
