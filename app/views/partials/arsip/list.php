
<?php

$comp_model = new SharedController;

$csrf_token = Csrf :: $token;

//Page Data From Controller
$view_data = $this->view_data;

$records = $view_data->records;
$record_count = $view_data->record_count;
$total_records = $view_data->total_records;

$field_name = Router :: $field_name;
$field_value = Router :: $field_value;

$view_title = $this->view_title;
$show_header = $this->show_header;
$show_footer = $this->show_footer;
$show_pagination = $this->show_pagination;


?>

<section class="page">
    
    <?php
    if( $show_header == true ){
    ?>
    
    <div  class="bg-light p-3 mb-3">
        <div class="container-fluid">
            
            <div class="row ">
                
                <div class="col-md-4 ">
                    <h3 class="record-title">Arsip</h3>
                    
                </div>
                
                <div class="col-md-4 comp-grid">
                    
                    <a  class="btn btn-primary" href="<?php print_link("arsip/add") ?>">
                        <i class="fa fa-plus "></i>                             
                        Tambah Arsip 
                    </a>
                    
                </div>
                
                <div class="col-md-4 comp-grid">
                    
                    <form  class="search" method="get">
                        <div class="input-group">
                            <input value="<?php echo get_query_str_value('search'); ?>" class="form-control" type="text" name="search"  placeholder="Cari Berkas" />
                                <div class="input-group-append">
                                    <button class="btn btn-primary"><i class="fa fa-search"></i></button>
                                </div>
                            </div>
                        </form>
                        
                    </div>
                    
                </div>
            </div>
        </div>
        
        <?php
        }
        ?>
        
        <div  class="">
            <div class="container-fluid">
                
                <div class="row ">
                    
                    <div class="col-md-12 comp-grid">
                        
                        <?php $this :: display_page_errors(); ?>
                        
                        <div  class=" animated fadeIn">
                            <div id="arsip-list-records">
                                
                                <?php
                                if(!empty($records)){
                                ?>
                                <div class="page-records table-responsive">
                                    <table class="table  table-striped table-sm">
                                        <thead class="table-header bg-light">
                                            <tr>
                                                
                                                <th class="td-sno td-checkbox">
                                                    <label class="custom-control custom-checkbox custom-control-inline">
                                                        <input class="toggle-check-all custom-control-input" type="checkbox" />
                                                        <span class="custom-control-label"></span>
                                                    </label>
                                                </th>
                                                
                                                <th class="td-sno">#</th>
                                                
                                                <th  <?php echo (get_query_str_value('orderby')=='NB' ? 'class="sortedby"' : null); ?>>
                                                    
                                                    <?php Html :: get_field_order_link('NB', "Nomor Berkas"); ?>
                                                </th>
                                                
                                                
                                                
                                                <th  <?php echo (get_query_str_value('orderby')=='SUBKK' ? 'class="sortedby"' : null); ?>>
                                                    
                                                    <?php Html :: get_field_order_link('SUBKK', "Kode Klasifikasi"); ?>
                                                </th>
                                                
                                                
                                                <th  <?php echo (get_query_str_value('orderby')=='UI' ? 'class="sortedby"' : null); ?>>
                                                    
                                                    <?php Html :: get_field_order_link('UI', "Uraian Berkas"); ?>
                                                </th>
                                                
                                                
                                                <th  <?php echo (get_query_str_value('orderby')=='TGL' ? 'class="sortedby"' : null); ?>>
                                                    
                                                    <?php Html :: get_field_order_link('TGL', "Tanggal Berkas"); ?>
                                                </th>
                                                
                                                
                                                <th  <?php echo (get_query_str_value('orderby')=='JMLH' ? 'class="sortedby"' : null); ?>>
                                                    
                                                    <?php Html :: get_field_order_link('JMLH', "Jumlah"); ?>
                                                </th>
                                                
                                                
                                                <th  <?php echo (get_query_str_value('orderby')=='KT' ? 'class="sortedby"' : null); ?>>
                                                    
                                                    <?php Html :: get_field_order_link('KT', "Keterangan"); ?>
                                                </th>
                                                
                                                
                                                <th  <?php echo (get_query_str_value('orderby')=='ST' ? 'class="sortedby"' : null); ?>>
                                                    
                                                    <?php Html :: get_field_order_link('ST', "Status"); ?>
                                                </th>
                                                
                                                <th > Daftar Isi Berkas</th>
                                                
                                                <th class="td-btn"></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            
                                            <?php
                                            $counter = 0;
                                            
                                            foreach($records as $data){
                                            $rec_id = (!empty($data['ID']) ? urlencode($data['ID']) : null);
                                            $counter++;
                                            
                                            
                                            ?>
                                            <tr>
                                                
                                                <th class=" td-checkbox">
                                                    <label class="custom-control custom-checkbox custom-control-inline">
                                                        <input class="optioncheck custom-control-input" name="optioncheck[]" value="<?php echo $data['ID'] ?>" type="checkbox" />
                                                            <span class="custom-control-label"></span>
                                                        </label>
                                                    </th>
                                                    
                                                    <th class="td-sno"><?php echo $counter; ?></th>
                                                    
                                                    
                                                    <td>
                                                        <a  data-source='<?php echo json_encode_quote(Menu :: $NB); ?>' 
                                                            data-value="<?php echo $data['NB']; ?>" 
                                                            data-pk="<?php echo $data['ID'] ?>" 
                                                            data-url="<?php print_link("arsip/editfield/" . urlencode($data['ID'])); ?>" 
                                                            data-name="NB" 
                                                            data-title="Enter Nb" 
                                                            data-placement="left" 
                                                            data-toggle="click" 
                                                            data-type="text" 
                                                            data-mode="popover" 
                                                            data-showbuttons="left" 
                                                            class="is-editable" >
                                                            <?php echo $data['NB']; ?>  
                                                        </a>
                                                    </td>
                                                    
                                                    
                                                    
                                                    
                                                    
                                                    <td>
                                                        <a  data-source='<?php 
                                                            $dependent_field = (!empty($data['KK']) ? urlencode($data['KK']) : null);
                                                            print_link('api/json/arsip_SUBKK_option_list/'.$dependent_field); 
                                                            ?>' 
                                                            data-value="<?php echo $data['SUBKK']; ?>" 
                                                            data-pk="<?php echo $data['ID'] ?>" 
                                                            data-url="<?php print_link("arsip/editfield/" . urlencode($data['ID'])); ?>" 
                                                            data-name="SUBKK" 
                                                            data-title="Select a value ..." 
                                                            data-placement="left" 
                                                            data-toggle="click" 
                                                            data-type="select" 
                                                            data-mode="popover" 
                                                            data-showbuttons="left" 
                                                            class="is-editable" >
                                                            <?php echo $data['SUBKK']; ?>  
                                                        </a>
                                                    </td>
                                                    
                                                    
                                                    
                                                    
                                                    <td><div><?php echo $data['UI']; ?></div>
                                                    </td>
                                                    
                                                    
                                                    
                                                    
                                                    <td>
                                                        <a  data-source='<?php echo json_encode_quote(Menu :: $NB); ?>' 
                                                            data-value="<?php echo $data['TGL']; ?>" 
                                                            data-pk="<?php echo $data['ID'] ?>" 
                                                            data-url="<?php print_link("arsip/editfield/" . urlencode($data['ID'])); ?>" 
                                                            data-name="TGL" 
                                                            data-title="Enter Tgl" 
                                                            data-placement="left" 
                                                            data-toggle="click" 
                                                            data-type="date" 
                                                            data-mode="popover" 
                                                            data-showbuttons="left" 
                                                            class="is-editable" >
                                                            <?php echo $data['TGL']; ?>  
                                                        </a>
                                                    </td>
                                                    
                                                    
                                                    
                                                    
                                                    <td>
                                                        <a  data-step="1" 
                                                            data-source='<?php echo json_encode_quote(Menu :: $NB); ?>' 
                                                            data-value="<?php echo $data['JMLH']; ?>" 
                                                            data-pk="<?php echo $data['ID'] ?>" 
                                                            data-url="<?php print_link("arsip/editfield/" . urlencode($data['ID'])); ?>" 
                                                            data-name="JMLH" 
                                                            data-title="Enter Jmlh" 
                                                            data-placement="left" 
                                                            data-toggle="click" 
                                                            data-type="number" 
                                                            data-mode="popover" 
                                                            data-showbuttons="left" 
                                                            class="is-editable" >
                                                            <?php echo $data['JMLH']; ?>  
                                                        </a>
                                                    </td>
                                                    
                                                    
                                                    
                                                    
                                                    <td>
                                                        <a  data-source='<?php print_link('api/json/arsip_KT_option_list'); ?>' 
                                                            data-value="<?php echo $data['KT']; ?>" 
                                                            data-pk="<?php echo $data['ID'] ?>" 
                                                            data-url="<?php print_link("arsip/editfield/" . urlencode($data['ID'])); ?>" 
                                                            data-name="KT" 
                                                            data-title="Select a value ..." 
                                                            data-placement="left" 
                                                            data-toggle="click" 
                                                            data-type="select" 
                                                            data-mode="popover" 
                                                            data-showbuttons="left" 
                                                            class="is-editable" >
                                                            <?php echo $data['KT']; ?>  
                                                        </a>
                                                    </td>
                                                    
                                                    
                                                    
                                                    
                                                    <td>
                                                        <a  data-source='<?php print_link('api/json/arsip_ST_option_list'); ?>' 
                                                            data-value="<?php echo $data['ST']; ?>" 
                                                            data-pk="<?php echo $data['ID'] ?>" 
                                                            data-url="<?php print_link("arsip/editfield/" . urlencode($data['ID'])); ?>" 
                                                            data-name="ST" 
                                                            data-title="Select a value ..." 
                                                            data-placement="left" 
                                                            data-toggle="click" 
                                                            data-type="select" 
                                                            data-mode="popover" 
                                                            data-showbuttons="left" 
                                                            class="is-editable" >
                                                            <?php echo $data['ST']; ?>  
                                                        </a>
                                                    </td>
                                                    
                                                    
                                                    
                                                    
                                                    <td>
                                                        <?php
                                                        $page_fields = array('NB' => $data['NB'],'KK' => $data['KK'],'SUBKK' => $data['SUBKK'],'TGL' => $data['TGL'],'KT' => $data['KT'],'ST' => $data['ST']);
                                                        $page_link = "masterdetail/index/arsip/daftar_berkas/NB/" . urlencode($data['NB']);
                                                        $md_pagelink = set_page_link($page_link, $page_fields); 
                                                        ?>
                                                        <a size="sm" class="btn btn-sm btn-primary page-modal" href="<?php print_link($md_pagelink) ?>">
                                                            <i class="fa fa-arrow-circle-down "></i> <?php echo $data['Daftar_Isi_Berkas'] ?>
                                                        </a>
                                                    </td>
                                                    
                                                    
                                                    
                                                    
                                                    <th class="td-btn">
                                                        
                                                        
                                                        
                                                        <a class="btn btn-sm btn-info has-tooltip" title="Edit This Record" href="<?php print_link("arsip/edit/$rec_id"); ?>">
                                                            <i class="fa fa-edit"></i> 
                                                        </a>
                                                        
                                                        
                                                        <a class="btn btn-sm btn-danger has-tooltip record-delete-btn" title="Delete this record" href="<?php print_link("arsip/delete/$rec_id/?csrf_token=$csrf_token"); ?>" data-prompt-msg="Anda Yakin Ingin Menghapus ?" data-display-style="modal">
                                                            <i class="fa fa-times"></i>
                                                            
                                                        </a>
                                                        
                                                        
                                                    </th>
                                                </tr>
                                                <?php 
                                                }
                                                ?>
                                                
                                                
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                    <?php
                                    if( $show_footer == true ){
                                    ?>
                                    <div class="">
                                        <div class="row">   
                                            <div class="col-sm-4">  
                                                <div class="py-2">  
                                                    
                                                    <button data-prompt-msg="Are you sure you want to delete these records?" data-display-style="modal" data-url="<?php print_link("arsip/delete/{sel_ids}/?csrf_token=$csrf_token"); ?>" class="btn btn-sm btn-danger btn-delete-selected d-none">
                                                        <i class="fa fa-times"></i> Delete Selected
                                                    </button>
                                                    
                                                    
                                                    <button class="btn btn-sm btn-primary export-btn"><i class="fa fa-save"></i> </button>
                                                    
                                                    
                                                    <?php Html :: import_form('arsip/import_data' , "", 'CSV , JSON'); ?>
                                                    
                                                </div>
                                            </div>
                                            <div class="col">   
                                                
                                                <?php
                                                if( $show_pagination == true ){
                                                $pager = new Pagination($total_records,$record_count);
                                                $pager->page_name='arsip';
                                                $pager->show_page_count=true;
                                                $pager->show_record_count=true;
                                                $pager->show_page_limit=true;
                                                $pager->show_page_number_list=true;
                                                $pager->pager_link_range=5;
                                                
                                                $pager->render();
                                                }
                                                ?>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                    }
                                    }
                                    else{
                                    ?>
                                    <div class="text-muted animated bounce  p-3">
                                        <h4><i class="fa fa-ban"></i> </h4>
                                    </div>
                                    <?php
                                    }
                                    ?>
                                    
                                </div>
                            </div>
                            
                        </div>
                        
                    </div>
                </div>
            </div>
            
        </section>
        