
<?php
$comp_model = new SharedController;

$csrf_token = Csrf :: $token;

//Page Data Information from Controller
$data = $this->view_data;

//$rec_id = $data['__tableprimarykey'];
$page_id = Router::$page_id; //Page id from url

$view_title = $this->view_title;

$show_header = $this->show_header;
$show_edit_btn = $this->show_edit_btn;
$show_delete_btn = $this->show_delete_btn;
$show_export_btn = $this->show_export_btn;

?>

<section class="page">
    
    <?php
    if( $show_header == true ){
    ?>
    
    <div  class="bg-light p-3 mb-3">
        <div class="container">
            
            <div class="row ">
                
                <div class="col-12 ">
                    <h3 class="record-title">View  Daftar Berkas</h3>
                    
                </div>
                
            </div>
        </div>
    </div>
    
    <?php
    }
    ?>
    
    <div  class="">
        <div class="container">
            
            <div class="row ">
                
                <div class="col-md-12 comp-grid">
                    
                    <?php $this :: display_page_errors(); ?>
                    
                    <div  class=" animated fadeIn">
                        <?php
                        
                        $counter = 0;
                        if(!empty($data)){
                        $rec_id = (!empty($data['ID']) ? urlencode($data['ID']) : null);
                        
                        
                        
                        $counter++;
                        ?>
                        <div class="page-records ">
                            <table class="table table-hover table-borderless table-striped">
                                <!-- Table Body Start -->
                                <tbody>
                                    
                                    <tr>
                                        <th class="title"> Id :</th>
                                        <td class="value"> <?php echo $data['ID']; ?> </td>
                                    </tr>
                                    
                                    
                                    <tr>
                                        <th class="title"> Nb :</th>
                                        <td class="value"> <?php echo $data['NB']; ?> </td>
                                    </tr>
                                    
                                    
                                    <tr>
                                        <th class="title"> Kk :</th>
                                        <td class="value"> <?php echo $data['KK']; ?> </td>
                                    </tr>
                                    
                                    
                                    <tr>
                                        <th class="title"> Subkk :</th>
                                        <td class="value"> <?php echo $data['SUBKK']; ?> </td>
                                    </tr>
                                    
                                    
                                    <tr>
                                        <th class="title"> Nomoritem :</th>
                                        <td class="value"> <?php echo $data['NOMORITEM']; ?> </td>
                                    </tr>
                                    
                                    
                                    <tr>
                                        <th class="title"> Nouritem :</th>
                                        <td class="value"> <?php echo $data['NOURITEM']; ?> </td>
                                    </tr>
                                    
                                    
                                    <tr>
                                        <th class="title"> Ui :</th>
                                        <td class="value"> <?php echo $data['UI']; ?> </td>
                                    </tr>
                                    
                                    
                                    <tr>
                                        <th class="title"> Tgl :</th>
                                        <td class="value"> <?php echo $data['TGL']; ?> </td>
                                    </tr>
                                    
                                    
                                    <tr>
                                        <th class="title"> Jmlh :</th>
                                        <td class="value"> <?php echo $data['JMLH']; ?> </td>
                                    </tr>
                                    
                                    
                                    <tr>
                                        <th class="title"> Kt :</th>
                                        <td class="value"> <?php echo $data['KT']; ?> </td>
                                    </tr>
                                    
                                    
                                    <tr>
                                        <th class="title"> St :</th>
                                        <td class="value"> <?php echo $data['ST']; ?> </td>
                                    </tr>
                                    
                                    
                                    <tr>
                                        <th class="title"> File :</th>
                                        <td class="value"><?php Html :: page_link_file($data['FILE']); ?></td>
                                    </tr>
                                    
                                    
                                    <tr>
                                        <th class="title"> Barcode :</th>
                                        <td class="value"> <?php echo $data['BARCODE']; ?> </td>
                                    </tr>
                                    
                                    
                                </tbody>
                                <!-- Table Body End -->
                                <tfoot>
                                    <tr>
                                        
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                        <div class="p-3">
                            
                            
                            <a class="btn btn-sm btn-info"  href="<?php print_link("daftar_berkas/edit/$rec_id"); ?>">
                                <i class="fa fa-edit"></i> 
                            </a>
                            
                            
                            <a class="btn btn-sm btn-danger record-delete-btn"  href="<?php print_link("daftar_berkas/delete/$rec_id/?csrf_token=$csrf_token"); ?>" data-prompt-msg="Anda Yakin Ingin Menghapus ?" data-display-style="modal">
                                <i class="fa fa-times"></i> 
                            </a>
                            
                            
                            <button class="btn btn-sm btn-primary export-btn">
                                <i class="fa fa-save"></i> 
                            </button>
                            
                            
                        </div>
                        <?php
                        }
                        else{
                        ?>
                        <!-- Empty Record Message -->
                        <div class="text-muted p-3">
                            <i class="fa fa-ban"></i> No Record Found
                        </div>
                        <?php
                        }
                        ?>
                    </div>
                    
                </div>
                
            </div>
        </div>
    </div>
    
</section>
