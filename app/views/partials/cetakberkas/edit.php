
<?php
$comp_model = new SharedController;

$csrf_token = Csrf :: $token;

$data = $this->view_data;

//$rec_id = $data['__tableprimarykey'];
$page_id = Router :: $page_id;

$show_header = $this->show_header;
$view_title = $this->view_title;
$redirect_to = $this->redirect_to;

?>

<section class="page">
    
    <?php
    if( $show_header == true ){
    ?>
    
    <div  class="bg-light p-3 mb-3">
        <div class="container">
            
            <div class="row ">
                
                <div class="col-12 ">
                    <h3 class="record-title">Edit  Cetakberkas</h3>
                    
                </div>
                
            </div>
        </div>
    </div>
    
    <?php
    }
    ?>
    
    <div  class="">
        <div class="container">
            
            <div class="row ">
                
                <div class="col-md-7 comp-grid">
                    
                    <?php $this :: display_page_errors(); ?>
                    
                    <div  class=" animated fadeIn">
                        <form novalidate  id="" role="form" enctype="multipart/form-data"  class="form form-horizontal needs-validation" action="<?php print_link("cetakberkas/edit/$page_id/?csrf_token=$csrf_token"); ?>" method="post">
                            <div>
                                
                                
                                <div class="form-group ">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <label class="control-label" for="SEMESTER">Semester <span class="text-danger">*</span></label>
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="">
                                                <input id="ctrl-SEMESTER"  value="<?php  echo $data['SEMESTER']; ?>" type="text" placeholder="Enter Semester"  required="" name="SEMESTER"  class="form-control " />
                                                    
                                                    
                                                    
                                                </div>
                                                
                                                
                                            </div>
                                        </div>
                                    </div>
                                    
                                    
                                    
                                    
                                    <div class="form-group ">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <label class="control-label" for="TAHUN">Tahun <span class="text-danger">*</span></label>
                                            </div>
                                            <div class="col-sm-8">
                                                <div class="input-group">
                                                    <input id="ctrl-TAHUN" class="form-control datepicker  datepicker"  required="" value="<?php  echo $data['TAHUN']; ?>" type="datetime" name="TAHUN" placeholder="Enter Tahun" data-enable-time="false" data-min-date="" data-max-date="" data-date-format="Y-m-d" data-alt-format="Y" data-inline="false" data-no-calendar="false" data-mode="single" />
                                                        
                                                        
                                                        <div class="input-group-append">
                                                            <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                                        </div>
                                                        
                                                    </div>
                                                    
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        
                                        
                                        
                                        
                                        <div class="form-group ">
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <label class="control-label" for="UNITPENGOLAH">Unitpengolah <span class="text-danger">*</span></label>
                                                </div>
                                                <div class="col-sm-8">
                                                    <div class="">
                                                        
                                                        <select required=""  id="ctrl-UNITPENGOLAH" name="UNITPENGOLAH"  placeholder="Select a value ..."    class="custom-select" >
                                                            
                                                            <option value="">Select a value ...</option>
                                                            
                                                            
                                                            <?php
                                                            $rec = $data['UNITPENGOLAH'];
                                                            $UNITPENGOLAH_options = $comp_model -> cetakberkas_UNITPENGOLAH_option_list();
                                                            if(!empty($UNITPENGOLAH_options)){
                                                            foreach($UNITPENGOLAH_options as $arr){
                                                            $val = array_values($arr);
                                                            $selected = ( $val[0] == $rec ? 'selected' : null );
                                                            ?>
                                                            <option 
                                                                <?php echo $selected; ?> value="<?php echo $val[0]; ?>"><?php echo (!empty($val[1]) ? $val[1] : $val[0]); ?>
                                                            </option>
                                                            <?php
                                                            }
                                                            }
                                                            ?>
                                                            
                                                        </select>
                                                        
                                                        
                                                    </div>
                                                    
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        
                                        
                                        
                                        
                                        <div class="form-group ">
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <label class="control-label" for="PENANDATANGAN">Penandatangan <span class="text-danger">*</span></label>
                                                </div>
                                                <div class="col-sm-8">
                                                    <div class="">
                                                        
                                                        <select required=""  id="ctrl-PENANDATANGAN" name="PENANDATANGAN" data-load-path="<?php print_link('api/json/cetakberkas_JABATAN_option_list') ?>"
                                                            data-load-select-options="#JABATAN-datalist"  placeholder="Select a value ..."    class="custom-select" >
                                                            
                                                            <option value="">Select a value ...</option>
                                                            
                                                            
                                                            <?php
                                                            $rec = $data['PENANDATANGAN'];
                                                            $PENANDATANGAN_options = $comp_model -> cetakberkas_PENANDATANGAN_option_list();
                                                            if(!empty($PENANDATANGAN_options)){
                                                            foreach($PENANDATANGAN_options as $arr){
                                                            $val = array_values($arr);
                                                            $selected = ( $val[0] == $rec ? 'selected' : null );
                                                            ?>
                                                            <option 
                                                                <?php echo $selected; ?> value="<?php echo $val[0]; ?>"><?php echo (!empty($val[1]) ? $val[1] : $val[0]); ?>
                                                            </option>
                                                            <?php
                                                            }
                                                            }
                                                            ?>
                                                            
                                                        </select>
                                                        
                                                        
                                                    </div>
                                                    
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        
                                        
                                        
                                        
                                        <div class="form-group ">
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <label class="control-label" for="JABATAN">Jabatan <span class="text-danger">*</span></label>
                                                </div>
                                                <div class="col-sm-8">
                                                    <div class="">
                                                        <input id="ctrl-JABATAN"  value="<?php  echo $data['JABATAN']; ?>" type="text" placeholder="Enter Jabatan" list="JABATAN-datalist"  required="" name="JABATAN" data-load-path="<?php print_link('api/json/cetakberkas_NIP_option_list') ?>"
                                                            data-load-select-options="#NIP-datalist"  class="form-control " />
                                                            <datalist id="JABATAN-datalist">
                                                                <?php 
                                                                $JABATAN_options = $comp_model -> cetakberkas_JABATAN_option_list($data['PENANDATANGAN']);
                                                                if(!empty($JABATAN_options)){
                                                                foreach($JABATAN_options as $arr){
                                                                $val = array_values($arr);
                                                                ?>
                                                                <option value="<?php echo $val[0]; ?>"><?php echo (!empty($val[1]) ? $val[1] : $val[0]); ?></option>
                                                                <?php
                                                                }
                                                                }
                                                            ?></datalist> 
                                                            
                                                            
                                                        </div>
                                                        
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            
                                            
                                            
                                            <div class="form-group ">
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <label class="control-label" for="NIP">Nip <span class="text-danger">*</span></label>
                                                    </div>
                                                    <div class="col-sm-8">
                                                        <div class="">
                                                            <input id="ctrl-NIP"  value="<?php  echo $data['NIP']; ?>" type="text" placeholder="Enter Nip" list="NIP-datalist"  required="" name="NIP"  class="form-control " />
                                                                <datalist id="NIP-datalist">
                                                                    <?php 
                                                                    $NIP_options = $comp_model -> cetakberkas_NIP_option_list($data['JABATAN']);
                                                                    if(!empty($NIP_options)){
                                                                    foreach($NIP_options as $arr){
                                                                    $val = array_values($arr);
                                                                    ?>
                                                                    <option value="<?php echo $val[0]; ?>"><?php echo (!empty($val[1]) ? $val[1] : $val[0]); ?></option>
                                                                    <?php
                                                                    }
                                                                    }
                                                                ?></datalist> 
                                                                
                                                                
                                                            </div>
                                                            
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                
                                                
                                                
                                            </div>
                                            <div class="form-ajax-status"></div>
                                            <div class="form-group text-center">
                                                <button class="btn btn-primary" type="submit">
                                                    
                                                    <i class="fa fa-send"></i>
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                    
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    
                </section>
                