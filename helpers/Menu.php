<?php
/**
 * Menu Items
 * All Project Menu
 * @category  Menu List
 */

class Menu{
	
	
	public static $navbartopleft = array(
		array(
			'path' => 'home', 
			'label' => 'Home', 
			'icon' => ''
		),
		
		array(
			'path' => 'arsip', 
			'label' => 'Arsip', 
			'icon' => ''
		),
		
		array(
			'path' => 'daftar_berkas', 
			'label' => 'Daftar Berkas', 
			'icon' => ''
		),
		
		array(
			'path' => '', 
			'label' => 'Setting Berkas', 
			'icon' => '<i class="fa fa-cog "></i>','submenu' => array(
		array(
			'path' => 'penandatangan/list', 
			'label' => 'Penandatangan', 
			'icon' => ''
		),
		
		array(
			'path' => 'unitpengolah/list', 
			'label' => 'Unit Pengolah', 
			'icon' => ''
		),
		
		array(
			'path' => 'status/list', 
			'label' => 'Status Aktif', 
			'icon' => ''
		),
		
		array(
			'path' => 'keterangan/list', 
			'label' => 'Keterangan', 
			'icon' => ''
		)
	)
		),
		
		array(
			'path' => '', 
			'label' => 'Setting Kode Klasifikasi', 
			'icon' => '<i class="fa fa-cogs "></i>','submenu' => array(
		array(
			'path' => 'kode_kk', 
			'label' => 'Kode Kk', 
			'icon' => ''
		),
		
		array(
			'path' => 'master_kasifikasi/list', 
			'label' => 'Klasifikasi', 
			'icon' => '<i class="fa fa-dedent "></i>'
		),
		
		array(
			'path' => 'kode_subklasifikasi', 
			'label' => 'Kode Subklasifikasi', 
			'icon' => ''
		)
	)
		),
		
		array(
			'path' => 'cetakberkas', 
			'label' => 'Cetakberkas', 
			'icon' => ''
		)
	);

	
	
	public static $NB = array();

}